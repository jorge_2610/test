@extends('layout')

@section('title', "Agenda Médica")

@section('content')

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Settings</title>
  <link rel="stylesheet" href="{{ asset('css/styleSettings.css') }}">
</head>

  <div id='settings' ontouchstart>
    <input checked class='nav' name='nav' type='radio'>
    <span class='nav'>Perfil</span>
    <input class='nav' name='nav' type='radio'>
    <span class='nav'>Horario</span>
    <input class='nav' name='nav' type='radio'>
    <span class='nav'>Cuenta</span>
    <main class='content'>
      <section id='profile'>
        <form>
          <ul>
            <li class='large padding avatar'>
              <span style="background-image: url({{ asset('images/House.jpg') }});"></span>
              <div>
                <fieldset class='material-button'>
                  <div>
                    <button>Cambiar</button>
                  </div>
                </fieldset>
              </div>
            </li>
            <li>
              <fieldset class='material'>
                <div>
                  <input required type='text' value='Gregory'>
                  <label>Nombre</label>
                  <hr>
                </div>
              </fieldset>
            </li>
            <li>
              <fieldset class='material'>
                <div>
                  <input required type='text' value="House">
                  <label>Apellidos</label>
                  <hr>
                </div>
              </fieldset>
            </li>
            <li>
              <fieldset class='material'>
                <div>
                  <input required type='text' value="Medico Cirujano">
                  <label>Titulo</label>
                  <hr>
                </div>
              </fieldset>
            </li>
            <li>
              <fieldset class='material'>
                <div>
                  <input required type='number' value="3312354665">
                  <label>Telefono</label>
                  <hr>
                </div>
              </fieldset>
            </li>
            <li class='large'>
              <fieldset class='material'>
                <div>
                  <input required type='text' value="Enfermedades Infecciosas y Nefrología">
                  <label>Especializacion</label>
                  <hr>
                </div>
              </fieldset>
            </li>
            <li class='large padding'>
              <fieldset class='material-button center'>
                <div>
                  <input class='save' type='submit' value='Guardar'>
                </div>
              </fieldset>
            </li>
          </ul>
        </form>
      </section>

      <section id='account'>
        <form>
          <ul>
            <li>
              <fieldset class='material'>
                <div>
                  <label>Horario</label>
                  <hr>
                </div>
              </fieldset>
              <fieldset class='material'>
                <div>
                  <input type='time' value="07:00">
                  <input type='time' value="12:00">
                  <hr>
                </div>
              </fieldset>
            </li>
            <li>
              <fieldset class='material'>
                <div>
                  <label>Dias</label>
                  <hr>
                </div>
              </fieldset>
              
              <fieldset class='material-checkbox'>
                <div>
                  <input checked type='checkbox'>
                  <div class='check'>
                    <span>
                    </span>
                    <label>Lunes</label>
                  </div>
                </div>
                <hr>
                <div>
                  <input checked type='checkbox'>
                  <div class='check'>
                    <span>
                    </span>
                    <label>Martes</label>
                  </div>
                </div>
                <hr>
                <div>
                  <input checked type='checkbox'>
                  <div class='check'>
                    <span>
                    </span>
                    <label>Miercoles</label>
                  </div>
                </div>
                <hr>
                <div>
                  <input checked type='checkbox'>
                  <div class='check'>
                    <span>
                    </span>
                    <label>Jueves</label>
                  </div>
                </div>
                <hr>
                <div>
                  <input type='checkbox'>
                  <div class='check'>
                    <span>
                    </span>
                    <label>Sabado</label>
                  </div>
                </div>
                <hr>
                <div>
                  <input type='checkbox'>
                  <div class='check'>
                    <span>
                    </span>
                    <label>Domingo</label>
                  </div>
                </div>
              </fieldset>

              <li class='large padding'>
                <fieldset class='material-button center'>
                  <div>
                    <input class='save' type='submit' value='Actualizar'>
                  </div>
                </fieldset>
              </li>
            </ul>
          </form>
        </section>


        <section id='account'>
          <form>
            <ul>
              <fieldset class='material'>
                <div>
                  <label>Rellene solo el campo que desea actualizar</label>
                  <hr>
                </div>
              </fieldset>
              <li>
                <fieldset class='material'>
                  <div>
                    <input required type='text' value='House@hotmail.com'>
                    <label>Correo Electronico Actual</label>
                    <hr>
                  </div>
                </fieldset>
              </li>
              <li>
                <fieldset class='material'>
                  <div>
                    <input required type='text'>
                    <label>Nuevo Correo Electronico</label>
                    <hr>
                  </div>
                </fieldset>
              </li>
              <li>
                <fieldset class='material'>
                  <div>
                    <input required type='password'>
                    <label>Nueva Contraseña</label>
                    <hr>
                  </div>
                </fieldset>
              </li>
              <li>
                <fieldset class='material'>
                  <div>
                    <input required type='password'>
                    <label>Confirmar Nueva Contraseña</label>
                    <hr>
                  </div>
                </fieldset>
              </li>
              <li class='large padding'>
                <fieldset class='material-button center'>
                  <div>
                    <input class='save' type='submit' value='Enviar Solicitud'>
                  </div>
                </fieldset>
              </li>
            </ul>
            <ul>

              <fieldset class='material'>
                <div>
                  <label>Se analizara su solicitud, y posteriormente se hara la modificacion pertinente.</label>
                  <hr>
                </div>
              </fieldset>

            </ul>
          </form>
        </section>
      </main>
    </div>
@endsection