@extends('layout')

@section('title', "Agenda Médica")

@section('content')

<head>
  <meta charset="UTF-8">
  <title>Agenda Medica</title>
  <link rel="stylesheet" href="{{ asset('css/styleAgenda.css') }}">

</head>

<body>

  <body data-spy="scroll" data-target=".scrollspy">
    
    <div class="">
      <div class="row trip">
        <div class="col-lg-11 col-md-pull-1 col-md-11 col-xs-10">
          <section id="Febrero" class="week"><span class="week__number">Febrero</span>
            <div class="row">
              <div class="col-lg-2">
                <h2 id="22FEB" class="panel__date">22</h2>
                <p class="panel__day">Jueves</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 09:30 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Juan Ayala Lopez </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 11:00 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Griscel Garcia Gonzalez </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 12:00 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Jorge Manuel Contreras Aviña </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 1:30 pm </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: El Bamby </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 02:30 pm </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Jesus Roldan Ortiz </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 04:30 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Alejandro Salgado Ramirez </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div class="row">
              <div class="col-lg-2">
                <h2 id="23FEB" class="panel__date">23</h2>
                <p class="panel__day">Viernes</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 08:30 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 23/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Carlos Arnulfo </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 10:30 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 23/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: David Alberto Torres </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 12:30 </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 22/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Karla Guadalupe Casillas Ramirez </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-2">
                <h2 id="24FEB" class="panel__date">24</h2>
                <p class="panel__day">Sabado</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 10:30 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 24/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Fernando Martin Jaime Ramirez </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-2">
                <h2 id="26FEB" class="panel__date">26</h2>
                <p class="panel__day">Domingo</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 02:00 pm </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 26/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Erick Castañeda </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-2">
                <h2 id="27FEB" class="panel__date">27</h2>
                <p class="panel__day">Martes</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 01:00 pm </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 27/Febrero/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Alan Ugalde </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- week 1 -->
          <section id="Marzo" class="week"><span class="week__number">Marzo</span>
            <div class="row">
              <div class="col-lg-2">
                <h2 id="01MAR" class="panel__date">01</h2>
                <p class="panel__day">Jueves</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 11:00 am </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 01/Marzo/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Griscel Garcia Gonzalez </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-2">
                <h2 id="02MAR" class="panel__date">02</h2>
                <p class="panel__day">Viernes</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 05:00 pm </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 02/Marzo/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Rocky la roca </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </section>
          <!-- week 2 -->

          <section id="Abril" class="week"><span class="week__number">Abril</span>
            <div class="row">
              <div class="col-lg-2">
                <h2 id="10ABR" class="panel__date">10</h2>
                <p class="panel__day">Martes</p>
              </div>
              <div class="col-lg-10 panel">
                <div class="agenda">
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> 02:00 pm </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      Fecha: 10/Abril/2018
                    </div>
                  </div>
                  <div class="row agenda__head">
                    <div class="col-lg-8">
                      <h4> Paciente: Adrian Everardo Segura Garcia </h4>
                    </div>
                    <div class="col-lg-4 panel__title--sub ">
                      <a href="">Ver Detalles </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    <script src="{{ URL::asset('js/jquery-2.1.3.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
  </body>
  @endsection
