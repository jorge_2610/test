@extends('layout')

@section('title', 'Pacientes')

@section('content')
    <h1>{{ $title }}</h1>

    <ul>
        @forelse ($patients as $patient)
            <li>
            	{{ $patient-> name }}, ({{ $patient-> email }})
            	<a href="{{ route('patients.show', ['id' => $patient-> id]) }}">Ver detalles</a>
            </li>
        @empty
            <li>No hay Pacientes Registrados.</li>
        @endforelse
    </ul>
@endsection