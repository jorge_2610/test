@extends('layout')

@section('title', "Paciente {$patient-> id}")

@section('content')
<link href="{{ asset('css/patient-details.css') }}" rel="stylesheet">
<div id="top">
<div id="cv" class="instaFade">
    <div class="mainDetails">
        <div id="headshot" class="quickFade">
            {{-- <img src="headshot.jpg" alt="Alan Smith" /> --}}
        </div>
        
        <div id="name">
            <h1 class="quickFade delayTwo">Expediente</h1>
            <h2 class="quickFade delayThree">{{$patient-> name}}</h2>
        </div>
        
        <div id="contactDetails" class="quickFade delayFour">
            <ul>
                <li>Fecha y Hora: {{$patient->created_at}}</a></li>
                <li>e: <a href="mailto:joe@bloggs.com" target="_blank">{{$patient->email}}</a></li>
                <li>m: 01234567890</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    
    <div id="mainArea" class="quickFade delayFive">
        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Información General</h1>
                </div>
                
                <div class="sectionContent">
                    <p>{{$patient->description}}.</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>
        
        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Diagnóstico</h1>
                </div>
                
                <div class="sectionContent">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dolor metus, interdum at scelerisque in, porta at lacus. Maecenas dapibus luctus cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim. Vestibulum bibendum mattis dignissim. Proin id sapien quis libero interdum porttitor.</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>
        
        <section>
            <div class="sectionTitle">
                <h1>Citas y Evoluciones</h1>
            </div>
            
            <div class="sectionContent">
                <article>
                    <h2>Cita No. X</h2>
                    <p class="subDetails">April 2011</p>
                    <p>Resultados de Consulta - Análisis*- Receta*.</p>
                    <h2>Evolución No. X</h2>
                    <p class="subDetails">April 2011</p>
                    <p>Resultados de Consulta - Análisis*- Receta*.</p>
                </article>
            </div>
            <div class="clear"></div>
        </section>

        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Padecimientos</h1>
                </div>
                
                <div class="sectionContent">
                    <p>Presenta ansiedad, depresión y trastornos de sueño(INSOMNIO)</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>
        
        
        <section>
            <div class="sectionTitle">
                <h1>Signos Vitales</h1>
            </div>
            
            <div class="sectionContent">
                <ul class="keySkills">
                    <li>Signo X</li>
                    <li>Signo X</li>
                    <li>Signo X</li>
                    <li>Signo X</li>
                </ul>
            </div>
            <div class="clear"></div>
        </section>
        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Pronóstico</h1>
                </div>
                
                <div class="sectionContent">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dolor metus, interdum at scelerisque in, porta at lacus. Maecenas dapibus luctus cursus.</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>
        
        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Plan de Estudio o Tratamiento</h1>
                </div>
                
                <div class="sectionContent">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dolor metus, interdum at scelerisque in, porta at lacus. Maecenas dapibus luctus cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit.</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>

        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Datos del médico Profesional</h1>
                </div>
                
                <div class="sectionContent">
                    <p>Nombre<br>Cédula Profesional<br>General Info: <br>Firma</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>
        
    </div>
</div>
</div>
@endsection