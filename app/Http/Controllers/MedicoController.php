<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MedicoController extends Controller
{
    //
	public function Settings(){
		return view('Medico.Settings');
	}

	public function Agenda(){
		return view('Medico.Agenda');
	}

}
