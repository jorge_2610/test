<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    public function index()
    {
        //$patients = DB::table('patient')->get();
        $patients = Patient::all();

        $title = 'Listado de Pacientes';

//        return view('patients.index')
//            ->with('patients', Patient::all())
//            ->with('title', 'Listado de usuarios');

        return view('patients.index', compact('title', 'patients'));
    }

    public function show($id)
    {
        $patient = Patient::find($id);

        return view('patients.show', compact('patient'));
    }
}
