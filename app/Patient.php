<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{

	protected $fillable = ['description'];

    public static function findByEmail($email)
    {
        return static::where(compact('email'))->first();
    }
}
