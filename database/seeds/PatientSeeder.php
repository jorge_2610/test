<?php

use App\Patient;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Patient::class)->create([
            'name' => 'Jorge Contreras',
            'email' => 'jorge@styde.net',
            'age' => '20',
            'description' => 'Medicyte Developer',
        ]);

        factory(Patient::class, 48)->create();
    }
}
