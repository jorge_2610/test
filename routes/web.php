<?php

Route::get('/', function () {
	return view('welcome');
});

Route::get('/pacientes', 'PatientController@index')
    ->name('patients.index');

Route::get('/pacientes/{id}', 'PatientController@show')
    ->where('id', '[0-9]+')
    ->name('patients.show');

Route::get('/Settings', 'MedicoController@Settings');

Route::get('/Agenda', 'MedicoController@Agenda');
